import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WorkWithStrings {
    public static void main(String[] args) {
        String stringToReplace = "Object-oriented programming bla bla. object-oriented programming bla bla.Object-oriented programming bla bla.Object-oriented programming bla bla.Object-oriented programming bla bla.";
        String replaceFromWord = "Object-oriented programming";
        String replaceToWord = "OOP";
        String findMinDistinctOrLatin = "ab fff ff f 1234 jks";
        String findPalindrome = "123 121 abc abba";
        System.out.println("Original string:");
        System.out.println(stringToReplace);
        System.out.println("String with replaced recurring phrase:");
        System.out.println(replaceRepetitions(stringToReplace, replaceFromWord, replaceToWord));
        System.out.println("First word with minimum distinct letters:");
        System.out.println(minDistinctLettersWord(findMinDistinctOrLatin));
        System.out.println("Count of words contain only latin letters:");
        System.out.println(latinWordCounter(findMinDistinctOrLatin));
        System.out.println("Palindromes list:");
        System.out.println(palindromesSearcher(findPalindrome));
    }

    private static String replaceRepetitions(String originalString, String replaceFrom, String replaceTo) {
        replaceFrom = "(?i)%s".formatted(replaceFrom);
        StringBuilder resultString = new StringBuilder();
        Pattern pattern = Pattern.compile(replaceFrom);
        Matcher match = pattern.matcher(originalString);
        int count = 0;
        while (match.find()) {
            if (count++ % 2 == 1) {
                match.appendReplacement(resultString, replaceTo);
            }
        }
        match.appendTail(resultString);
        return resultString.toString();
    }

    private static String minDistinctLettersWord(String originalString) {
        List<String> myList = new ArrayList<>(Arrays.asList(originalString.split(" ")));
        return myList.stream().min(Comparator.comparing(word -> word.chars().distinct().count())).get();
    }

    private static int latinWordCounter(String originalString) {
        List<String> myList = new ArrayList<>(Arrays.asList(originalString.split(" ")));
        return (int) myList.stream().filter(word -> word.matches("^[a-zA-Z]+$")).count();
    }

    private static List<String> palindromesSearcher(String originalString) {
        List<String> myList = new ArrayList<>(Arrays.asList(originalString.split(" ")));
        myList.removeIf(word -> !isPalindrome(word));
        return myList;
    }

    private static boolean isPalindrome(String originalString) {
        StringBuilder reversedString = new StringBuilder(originalString);
        reversedString.reverse();
        return originalString.equals(reversedString.toString());
    }

}
